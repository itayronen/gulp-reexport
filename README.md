# gulp-reexport
A gulp plugin to create a single file that re-exports the input files stream.

## Install
`npm install --save-dev gulp-reexport`

## Basic Usage
gulpfile.js
```js
let reexport = require('gulp-reexport').default;

gulp.task("reexport", function () {
    return gulp.src("src", { read: false }) 
        .pipe(reexport("main.d.ts")) // "main.d.ts" - Output file name.
        .pipe(gulp.dest(outputDir))
        ;
});
```  
Or, with some more controll:
```js
var reexport = require('gulp-reexport').default;

gulp.task("Generate d.ts", function () {
    return gulp.src("<some glob to ts files>", { base: "<some relative path if needed>", read: false }) 
        .pipe(reexport("main.d.ts",
            {
                exportedBase: "<base path for the re-exported modules paths>",
                pathResolver: (file) => { return "the file path to export" },
                // Helpful when the output definition file is not in the root folder of the re-exported modules.
                // For example:
                /*
                    lib
                    -   main.js
                    -   main.d.ts       // The generated definitions.
                    -   MyLibCode
                        -   code1.js    
                        -   code1.d.ts  // Module definitions.
                        -   code2.js
                        -   code2.d.ts  // Module definitions.

                    In this example, the base for the modules is "MyLibCode".
                */ 
            }))
        .pipe(gulp.dest(outputDir))
        ;
});
```