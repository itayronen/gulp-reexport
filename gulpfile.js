var gulp = require("gulp");
var gulpIgnore = require("gulp-ignore");
var ts = require('gulp-typescript');
var del = require("del");
var changed = require("itay-gulp-changed");

var codeSource = "src/";
var tsconfigPath = codeSource + 'tsconfig.json';
var typescriptFilesGlob = [codeSource + "**/*.ts", "!**/*.d.ts"];
var testsDtsGlob = "*.test.d.ts";
var outputDir = require("./package.json").config.outputDir + "/";

var cleanOutputTask = "clean";

gulp.task(cleanOutputTask, () => {
	changed.reset();
	del([outputDir + '**/*']);
});

gulp.task("build", function () {
	var tsProject = ts.createProject(tsconfigPath, { declaration: true });

	var tsResult = gulp.src(typescriptFilesGlob, { base: codeSource })
		.pipe(changed())
		.pipe(tsProject());

	tsResult.dts
		.pipe(gulpIgnore.exclude(testsDtsGlob))
		.pipe(gulp.dest(outputDir));

	// Should merge
	return tsResult.js
		.pipe(gulp.dest(outputDir))
		;
});