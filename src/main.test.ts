import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import ostream, { Readable } from "o-stream";
import * as  gutil from "gulp-util";
import reexport, { createReexportsFileAsStream } from "./main";

function string_src(files: { filePath: any, content: any }[]): Readable {

	var gulpFiles: any[] = []

	files.forEach(file => {
		gulpFiles.push(new gutil.File({
			cwd: "",
			base: "",
			path: file.filePath,
			contents: new Buffer(file.content)
		}));
	});

	return ostream.fromArray(gulpFiles);
}

export default function (suite: TestSuite) {

	suite.describe("Reexport", (suite) => {

		suite.test("One file (not relative)", async t => {

			let expected = `export * from "file";\n`; // The new line at the end is mandatory for some reason.

			let stream = string_src([{ filePath: "file.ts", content: "export class A{}" }])
				.pipe(reexport("asd.d.ts"));

			await new Promise(resolve => setTimeout(resolve));

			let actual = stream.read().contents.toString();

			expect(actual).equal(expected);
		});

		suite.test("Multiple file", async t => {

			let expected = `export * from "file";\nexport * from "file1";\n`;

			let stream = string_src(
				[
					{ filePath: "file.ts", content: "export class A{}" },
					{ filePath: "file1.ts", content: "export class B{}" }
				])
				.pipe(reexport("asd"));

			await new Promise(resolve => setTimeout(resolve));

			let actual = stream.read().contents.toString();

			expect(actual).equal(expected);
		});

		suite.test("re-export with relative base", async t => {

			let expected = "export * from \"./baseFolder/file\";\n";

			let stream = string_src([{ filePath: "file.ts", content: "export class A{}" }])
				.pipe(reexport("result.ts", { exportedBase: "./baseFolder" }));

			await new Promise(resolve => setTimeout(resolve));

			let actual = stream.read().contents.toString();

			expect(actual).equal(expected);
		});

		suite.test("pathResolver option", async t => {

			let expected = "export * from \"./baseFolder/file\";\n";

			let stream = string_src([{ filePath: "file.ts", content: "export class A{}" }])
				.pipe(reexport("asd", { pathResolver: (file) => { return "./baseFolder/" + file.relative; } }));

			await new Promise(resolve => setTimeout(resolve));

			let actual = stream.read().contents.toString();

			expect(actual).equal(expected);
		});

		suite.test("pathResolver and base option", async t => {

			let expected = "export * from \"./aa/baseFolder/file\";\n";

			let stream = string_src([{ filePath: "file.ts", content: "export class A{}" }])
				.pipe(reexport("asd", {
					pathResolver: (file) => { return "./baseFolder/" + file.relative; },
					exportedBase: "./aa/"
				}));

			await new Promise(resolve => setTimeout(resolve));

			let actual = stream.read().contents.toString();

			expect(actual).equal(expected);
		});
	});

	suite.describe(createReexportsFileAsStream.name, (suite) => {

		suite.test("Integration", async t => {

			let expected = `export * from "../file1";\nexport * from "./myFolder/file2";\n`; // The new line at the end is mandatory for some reason.

			let stream = string_src(
				[
					{ filePath: "file1.ts", content: "export class A{}" },
					{ filePath: "out/myFolder/file2.ts", content: "export class A{}" },
				]).pipe(createReexportsFileAsStream("out", "asd.d.ts"));

			await new Promise(resolve => setTimeout(resolve));

			let actual = (stream.read() as any).contents.toString();

			expect(actual).equal(expected);
		});
	});
}