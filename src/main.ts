import ObjectStream, { EnteredArgs, EndedArgs, Transform } from "o-stream";
import * as gutil from "gulp-util";
import * as path from "path";

export default function reexport(outFilePath: string, options?: Options): Transform {
	let generator = new ExportsGenerator(options);
	let inputs: InputFile[] = [];

	return ObjectStream.transform({
		onEntered: (args: EnteredArgs<gutil.File, gutil.File>) => {
			inputs.push(new InputFile(args.object.path, args.object.relative));
		},
		onEnded: (args: EndedArgs<gutil.File>) => {
			let file = new gutil.File({
				cwd: "",
				base: "",
				path: outFilePath,
				contents: new Buffer(generator.generateExports(inputs))
			})

			args.output.push(file);
		}
	});
}

export function createReexportsFileAsStream(plannedOutDir: string, outFileName: string): NodeJS.ReadWriteStream {
	return ObjectStream.combineStreams(
		reexport(outFileName, {
			pathResolver: file => {
				let relative = path.relative(plannedOutDir, file.path);

				if (!relative.startsWith(".")) {
					relative = "./" + relative;
				}

				return relative;
			}
		}),
		ObjectStream.transform<gutil.File, gutil.File>({
			onEntered: args => {
				if ((args.object.contents as Buffer).length > 0) args.output.push(args.object);
			}
		}))
		;
}

export interface CreateReexportsFileParams {
	gulp: any;
	glob: string;
	outDir: string;
	outFileName: string;
	changed?: (params: { key: string, isGroupOperation: boolean }) => NodeJS.ReadableStream;
	changeKey?: string
}

export function createReexportsFileOnFileSystem(params: CreateReexportsFileParams): NodeJS.WritableStream {
	let changeKey = params.changeKey || params.outFileName + ".changeKey";

	let stream = params.gulp.src(params.glob, { read: false });

	if (params.changed) {
		stream = stream
			.pipe(params.changed({ key: changeKey, isGroupOperation: true }));
	}

	stream = stream
		.pipe(createReexportsFileAsStream(params.outDir, params.outFileName))
		.pipe(params.gulp.dest(params.outDir));

	return stream;
}

export class InputFile {
	constructor(public path: string, public relative: string) {

	}
}

export interface Options {
	exportedBase?: string;
	pathResolver?: ((path: InputFile) => string);
}

class ExportsGenerator {

	private options: Options;

	constructor(options?: Options) {
		this.options = options || {};

		this.options.exportedBase = this.options.exportedBase || "";
	}

	public generateExports(inputs: InputFile[]): string {
		var exportsStrings: string[] = [];

		for (var i = 0; i < inputs.length; i++) {

			let exportString = this.getFileExport(inputs[i]);

			exportsStrings.push(exportString);
		}

		return exportsStrings.join("");
	}

	private getFileExport(inputFile: InputFile): string {

		if (!inputFile) {
			throw new Error("Null or undefined input file.");
		}

		let exportedPath = inputFile.relative;

		if (this.options.pathResolver) {
			exportedPath = this.options.pathResolver(inputFile);
		}

		if (this.options.exportedBase!.length > 0) {
			let relativeBase = "";

			if (this.options.exportedBase!.indexOf('./') == 0) {
				relativeBase = "./";
			}
			else if (this.options.exportedBase!.indexOf('../')) {
				relativeBase = "../";
			}

			exportedPath = path.join(this.options.exportedBase!, exportedPath);
			exportedPath = relativeBase + exportedPath;
		}

		exportedPath = exportedPath.replace(/\\/g, "/");
		exportedPath = this.removeExtension(exportedPath);

		let exportString = `export * from "${exportedPath}";\n`;

		return exportString;
	}

	private removeExtension(filePath: string): string {
		let extension = path.extname(filePath);

		if (extension) {
			return filePath.substring(0, filePath.lastIndexOf(extension));
		}

		return filePath;
	}
}
